<?php

Route::get('/', 'TodoController@index');
Route::post('/todo', 'TodoController@store');
Route::delete('/todo/{todo}', 'TodoController@destroy');
