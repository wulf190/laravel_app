@extends('layouts.app')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            New Task
        </div>

        <div class="panel-body">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <!-- New Task Form -->
            <form action="{{ url('todo')  }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

                <!-- Task Name  -->
                <div class="form-group">
                    <label for="todo-title" class="col-sm-3 control-label">Todo</label>
                    <div class="col-sm-6">
                        <input type="text" name="title" id="todo-title" class="form-control">
                    </div>
                </div>

                <!-- Add Task Button -->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-btn fa-plus"></i>Add Task
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Current Todos -->
    @if (count($todos) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                Current Todos
            </div>

            <div class="panel-body">
                <table class="table table-striped todo-table">
                    <thead>
                    <th>Todo</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                    @foreach($todos as $todo)
                        <tr>
                            <td class="table-text"><div>{{ $todo->title }}</div></td>
                            <!-- Delete Todos -->
                            <td>
                                <form action="{{ url('todo/'.$todo->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button type="submit" class="btn btn-danger">
                                        <i class="fa fa-trash"></i> 削除
                                    </button>
                                </form>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection