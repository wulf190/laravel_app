<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;

/**
 * Todo class
 */
class Todo extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * Todo一覧表示
     *
     * Collection
     */
    public function getTodos(): Collection
    {
        $todos = $this->all();
        return $todos;
    }

    /**
     * Todo新規登録
     *
     * @param array
     */
    public function store($inputTitle)
    {
        $this->fill($inputTitle)->save();
    }

    /**
     * Todo削除
     *
     * @param integer $id
     */
    public function deleteTodo(int $id)
    {
        $this->destroy($id);
    }
}