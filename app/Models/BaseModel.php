<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * BaseModel class
 */
class BaseModel extends Model
{
    //
}