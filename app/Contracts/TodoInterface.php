<?php
declare(strict_types=1);

namespace App\Contracts;

/**
 * TodoInterface
 */
interface TodoInterface
{
    public function getTodos();
    public function store(array $inputTitle);
    public function deleteTodo(int $id);
}