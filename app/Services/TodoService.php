<?php
declare(strict_types=1);

namespace App\Services;

use App\Contracts\TodoInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

/**
 * TodoService class
 */
class TodoService
{

    /**
     * @var TodoInterface
     */
    private $todoInterface;

    /**
     * constructor
     *
     * @param TodoInterface $todo
     */
    public function __construct(TodoInterface $todo)
    {
        $this->todoInterface = $todo;
    }

    /**
     * Todo一覧取得
     *
     * @return Collection
     */
    public function getTodos(): Collection
    {
        return $this->todoInterface->getTodos();
    }

    /**
     * Todo新規登録
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        $inputTitle = $request->all('title');
        $this->todoInterface->store($inputTitle);
    }

    /**
     * Todo削除
     *
     * @param int $id
     */
    public function deleteTodo(int $id)
    {
        $this->todoInterface->deleteTodo($id);
    }
}