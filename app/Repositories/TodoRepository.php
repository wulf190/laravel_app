<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Models\Todo;
use App\Contracts\TodoInterface;
use Illuminate\Support\Collection;

/**
 * TodoRepository class
 */
class TodoRepository implements TodoInterface
{

    /**
     * @var Todo
     */
    private $todo;

    /**
     * constructor
     *
     * @param Todo $todo
     */
    public function __construct(Todo $todo)
    {
        $this->todo = $todo;
    }

    /**
     * Todo一覧取得
     *
     * @return Collection
     */
    public function getTodos(): Collection
    {
        return $this->todo->getTodos();
    }

    /**
     * Todo新規登録
     *
     * @param array
     */
    public function store(array $inputTitle)
    {
        $this->todo->store($inputTitle);
    }

    /**
     * Todo削除
     *
     * @param int $id
     */
    public function deleteTodo(int $id)
    {
        $this->todo->deleteTodo($id);
    }
}